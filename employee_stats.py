import json
import csv
import logging
import structlog


log = structlog.get_logger()
logging.basicConfig(level=logging.INFO)


def read_employee_csv(csvfile: str, delimiter: str=','):
	employee_data = []

	try:
		fid = open(csvfile, 'r')
		csv_reader = csv.reader(fid, delimiter=delimiter)
		darray = []
		for row in csv_reader:
			darray.append(row)

		fid.close()

		data_array = [darray[i] for i in range(1, len(darray))]
		log.info(f'csv file: {csvfile} read in sussessful')
		log.info(f'Number of records: {len(data_array)}')
		data_array= sorted(data_array, key=lambda x: x[1])
		employee_data = [darray[0]]
		for row in data_array:
			employee_data.append(row)

	except Exception as e:
		log.error(f'error read csv file {csvfile}: {str(e)}')

	return employee_data


def read_company_info(json_file: str):
	company_info = []

	try:
		fid = open(json_file, 'r')
		data = fid.read().split('\n')
		log.info(f'number of compony records {len(data)}')

		for rec in data:
			if len(rec) <2 : continue
			json_obj = json.loads(rec)
			company_info.append(json_obj)
		log.info(f'read company info successful: {json_file}')
		log.info(f'Number of data records in json file: {len(company_info)}')
	except Exception as e:
		log.error(f'error read json file {json_file}: {str(e)}')
	return company_info
	


def rearrange_company(company_info: list):
	# rearrange company info for fast search
	new_info = {}
	for comp in company_info:
		web = comp.get("website")
		if not web:
			log.error(f'website not found for {comp[0]}')
			continue
		new_info[web]= comp
	log.info(f'Number of new data records: {len(new_info)}')
	return new_info


def write_data_array(array: list, output: str, fmt: str = 'csv'):
	if fmt != 'csv':
		log.error('Only csv file format is supported, will use csv')
	try:
		f = open(output, 'w')
		writer = csv.writer(f)
		for row in array:
			writer.writerow(row)
		f.close()
		log.info(f'Data array written to: {output}')
	except Exception as e:
		log.error(f'Error write output to file: {output}, {str(e)}')



def find_company_employees(employee_data: list):
	emp_number = {}

	for r in range(1, len(employee_data)):
		row = employee_data[r]
		name = row[5]
		emp_number[name] = 0
	for r in range(1, len(employee_data)):
		row = employee_data[r]
		name = row[5]
		emp_number[name] += 1
	
	# print(emp_number)

	employee_list = []
	for key in emp_number.keys():
		rec = [key, emp_number[key]]
		employee_list.append(rec)
	
	employee_list = sorted(employee_list,key = lambda x: x[1],
		reverse = True)
	top10 = [employee_list[i] for i in range(0,10)]
	last = top10[9][1]
	
	for r in range(10,len(employee_list)):
		if int(employee_list[r][1]) < last:
			break
		top10.append(employee_list[r])
	
	return top10
			
		

	
def get_records(employee_file: str, company_file: str, outfile: str):
	emp_array = read_employee_csv(employee_file)
	print(emp_array[0], len(emp_array[0]))
	cmp_array = read_company_info(company_file)
	new_comp = rearrange_company(cmp_array)

	employee_table_head = emp_array[0]
	new_data = ['company_name', 'company_country',
				'company_region', 'company_year_founded']

	employee_table_head += new_data
	log.info(employee_table_head)
	new_records = [employee_table_head]
	
	for r in range(1, len(emp_array)):
		try:
			record = emp_array[r]
			email_list = record[3].split('@')
			company_address = email_list[1]
			company_obj = new_comp.get(company_address)
			company_name = company_obj['name']
			country =  company_obj['country']
			region = company_obj["region"]
			year = company_obj["year_founded"]
			ext_data = [company_name, country, region, year]
			record += ext_data
			new_records.append(record)
		except Exception as e:
			log.error(f'error handle record: {r}, {str(e)}')

	log.info(f'Number of new data length: {len(new_records)}')
	top10_rec = find_company_employees(new_records)
	log.info(f'companied of top 10 employee numbers: {top10_rec}')
	write_data_array(new_records, outfile, 'csv')


if __name__ == '__main__':
	get_records('employee.csv', 'company.json', 'employee_new_records.csv')












